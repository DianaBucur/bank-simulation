
public class SpendingAccount extends Account {
	private float amount;
	private String number;
	private String type;
	
	SpendingAccount(String number, float amount, String type) {
		super(number, amount, type);
	}
}
