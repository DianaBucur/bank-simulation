import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable, Observer {
	private String name;
	private String address;
	private String phoneno;
	
	Person(String name, String address, String phoneno){
		this.name = name;
		this.address = address;
		this.phoneno = phoneno;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhoneno() {
		return phoneno;
	}


	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void update(Account a, float b) {
		System.out.println("The account " + a.getNumber() + " of the holder " + this.getName() + "has been updated to the sum of " + b);
	}

	@Override
	public void update(Observable o, Object arg) {
		Account acc = (Account) o;
		System.out.println("The account " + acc.getNumber() + " of the holder " + this.getName() + "has been updated to the sum of " + (Float) arg);
		
	}
	
}
