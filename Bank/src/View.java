import java.awt.BorderLayout;
import java.util.Vector;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class View extends JFrame {

	private JPanel contentPane;

	public View() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 570, 425);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		Bank bank = new Bank();
		
		JButton btnNewButton = new JButton("Manage Persons");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame frame = new JFrame();
				frame.setVisible(true);
				frame.setBounds(100, 100, 835, 604);
				getContentPane().setLayout(null);
							

				JPanel panel = new JPanel();
				panel.setBounds(0, 0, 813, 532);
				getContentPane().add(panel);
				panel.setLayout(null);
				frame.add(panel);
				
				JTextField textField = new JTextField();
				textField.setBounds(575, 63, 198, 26);
				panel.add(textField);
				textField.setColumns(10);
				
				JTextField textField_1 = new JTextField();
				textField_1.setBounds(575, 130, 198, 26);
				panel.add(textField_1);
				textField_1.setColumns(10);
				
				JTextField textField_2 = new JTextField();
				textField_2.setBounds(575, 202, 198, 26);
				panel.add(textField_2);
				textField_2.setColumns(10);
				
				JTable table = new JTable();
				table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent arg0) {
						int row = table.getSelectedRow();
						int column = 0;
						String selected1 = table.getModel().getValueAt(row, column).toString();
						textField.setText(selected1);
						int column2 = 1;
						String selected2 = table.getModel().getValueAt(row, column2).toString();
						textField_1.setText(selected2);
						int column3 = 2;
						String selected3 = table.getModel().getValueAt(row, column3).toString();
						textField_2.setText(selected3);
						}
				});
				table.setBounds(15, 26, 420, 384);
				panel.add(table);
				
				
				JLabel lblNewLabel = new JLabel("Name: ");
				lblNewLabel.setBounds(450, 66, 69, 20);
				panel.add(lblNewLabel);
				
				JLabel lblNewLabel_1 = new JLabel("Address:");
				lblNewLabel_1.setBounds(450, 133, 69, 20);
				panel.add(lblNewLabel_1);
				
				JLabel lblNewLabel_2 = new JLabel("Phone number:");
				lblNewLabel_2.setBounds(450, 205, 110, 20);
				panel.add(lblNewLabel_2);
				
				JButton btnAddPerson = new JButton("Add Person");
				
				btnAddPerson.setBounds(531, 276, 156, 29);
				panel.add(btnAddPerson);
				
				JButton btnEditPerson = new JButton("Edit Person");
				btnEditPerson.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						String name = textField.getText();
						String address = textField_1.getText();
						String phone = textField_2.getText();
						Person person = bank.findPerson(name);
						person.setName(name);
						person.setAddress(address);
						person.setPhoneno(phone);
						bank.serialize();
					}
					
				});
				btnEditPerson.setBounds(531, 342, 156, 29);
				panel.add(btnEditPerson);
				
				JButton btnDeletePerson = new JButton("Delete Person");
				
				
				btnDeletePerson.setBounds(531, 405, 156, 29);
				panel.add(btnDeletePerson);
				
				JButton btnViewPersons = new JButton("View Persons");
				
				

				btnAddPerson.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						String name = textField.getText();
						String address = textField_1.getText();
						String phone = textField_2.getText();
						Person person = new Person(name, address, phone);
						bank.addPerson(person);
						bank.serialize();
					}			
				});
				
				btnDeletePerson.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						String name = textField.getText();
						Person person = bank.findPerson(name);
						bank.removePerson(person);
						bank.serialize();
					}
				});
				
				btnViewPersons.setBounds(135, 444, 129, 29);
				panel.add(btnViewPersons);
				btnViewPersons.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						DefaultTableModel model = new DefaultTableModel();
						model = bank.buildTableModel(table);
						table.setModel(model);
						JScrollPane scrollpane = new JScrollPane(table);
						scrollpane.setBounds(15, 26, 420, 384);
						panel.add(scrollpane);
					}
				});
				
			
			}
		});
		btnNewButton.setBounds(308, 158, 179, 49);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Manage Accounts");
		btnNewButton_1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {	
				JFrame frame = new JFrame();
				frame.setBounds(100, 100, 817, 573);
				getContentPane().setLayout(null);
				
				JPanel panel = new JPanel();
				panel.setBounds(0, 0, 795, 517);
				getContentPane().add(panel);
				panel.setLayout(null);
				frame.add(panel);
				

				JTextField textField = new JTextField();
				textField.setBounds(605, 94, 175, 26);
				panel.add(textField);
				textField.setColumns(10);
				
				JTextField textField_1 = new JTextField();
				textField_1.setBounds(605, 136, 175, 26);
				panel.add(textField_1);
				textField_1.setColumns(10);
				
				JTable table = new JTable();
				table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent arg0) {
						int row = 0;
						row = table.getSelectedRow();
						int column = 1;
						String selected1 = table.getModel().getValueAt(row, column).toString();
						textField.setText(selected1);
						int column2 = 2;
						String selected2 = table.getModel().getValueAt(row, column2).toString();
						textField_1.setText(selected2);
						}
				});
				table.setBounds(25, 29, 411, 364);
				panel.add(table);
				
				
				JButton btnViewAccounts = new JButton("View Accounts");
				btnViewAccounts.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						//DefaultTableModel model =  model.setRowCount(0);
						DefaultTableModel model = bank.buildTableModel2(table);
						table.setModel(model);
						JScrollPane scrollpane = new JScrollPane(table);
						scrollpane.setBounds(15, 26, 420, 384);
						panel.add(scrollpane);
					}
				});
				btnViewAccounts.setBounds(158, 434, 135, 29);
				panel.add(btnViewAccounts);
				
				
				JComboBox comboBox = new JComboBox();
				bank.fillComboBoxPers(comboBox);
				comboBox.setBounds(515, 29, 199, 26);
				panel.add(comboBox);
				
				
				
				JLabel lblAccountNumber = new JLabel("Account Number:");
				lblAccountNumber.setBounds(464, 97, 155, 20);
				panel.add(lblAccountNumber);
				
				
				JLabel lblNewLabel = new JLabel("Amount:");
				lblNewLabel.setBounds(464, 139, 69, 20);
				panel.add(lblNewLabel);
				
				JComboBox comboBox_1 = new JComboBox();
				comboBox_1.addItem("Spending Account");
				comboBox_1.addItem("Saving Account");
				comboBox_1.setBounds(515, 193, 199, 26);
				panel.add(comboBox_1);
				
				JButton btnAddAcount = new JButton("Add Account");
				btnAddAcount.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						String number = textField.getText();
						float amount = Float.parseFloat(textField_1.getText());
						String name = (String) comboBox.getSelectedItem();
						Person person = bank.findPerson(name);
						String t = (String) comboBox_1.getSelectedItem();
						String spend = "Spending Account";
						String save = "Saving Account";
						if(spend.equals(t)){
							SpendingAccount account = new SpendingAccount(number, amount, "Spending");
							account.addObserver(person);
							bank.addAccount(person, account);
						}else{ 
							SavingAccount account = new SavingAccount(number, amount, "Saving", "Open");
							account.addObserver(person);
							bank.addAccount(person, account);
						}
						bank.serialize();
					}			
				});
				
				btnAddAcount.setBounds(545, 289, 145, 29);
				panel.add(btnAddAcount);
				
				
				JButton btnNewButton = new JButton("Edit Account");
				btnNewButton.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						int row = table.getSelectedRow();
						int column = 1;
						String number = table.getModel().getValueAt(row, column).toString();
						String number2 = textField.getText();
						float amount2 = Float.parseFloat(textField_1.getText());
						Account acc = bank.findAccount(number);
						acc.setNumber(number2);
						acc.setAmount(amount2);
						bank.serialize();
					}
					
				});
				btnNewButton.setBounds(545, 344, 145, 29);
				panel.add(btnNewButton);
				
				JButton btnNewButton_1 = new JButton("Delete Account");
				btnNewButton_1.addActionListener(new ActionListener(){

					public void actionPerformed(ActionEvent e) {
						int row = table.getSelectedRow();
						int column = 1;
						String selected1 = table.getModel().getValueAt(row, column).toString();
						Account acc = bank.findAccount(selected1);
						bank.removeAccount(acc);
						bank.serialize();
						
					}
					
				});
				btnNewButton_1.setBounds(545, 405, 145, 29);
				panel.add(btnNewButton_1);
				frame.setVisible(true);
			}
			
		});
		btnNewButton_1.setBounds(65, 158, 179, 49);
		contentPane.add(btnNewButton_1);
		
		JButton button = new JButton("Withdraw/Add Money");
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				JFrame frame = new JFrame();
				frame.setBounds(100, 100, 745, 499);
				getContentPane().setLayout(null);

				JPanel panel = new JPanel();
				panel.setBounds(0, 0, 813, 532);
				getContentPane().add(panel);
				panel.setLayout(null);
				frame.add(panel);
				
				JComboBox comboBox_1 = new JComboBox();
				comboBox_1.setBounds(347, 133, 194, 26);
				panel.add(comboBox_1);
				
				JComboBox comboBox = new JComboBox();
				bank.fillComboBoxPers(comboBox);
				comboBox.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						bank.fillComboBoxAcc(comboBox_1, (String) comboBox.getSelectedItem());
					}
					
				});
				comboBox.setBounds(347, 70, 194, 26);
				panel.add(comboBox);
				
				JLabel lblSelectHolder = new JLabel("Select Holder:");
				lblSelectHolder.setBounds(185, 73, 110, 20);
				panel.add(lblSelectHolder);
				
				JLabel lblSelectAccount = new JLabel("Select Account:");
				lblSelectAccount.setBounds(185, 136, 129, 20);
				panel.add(lblSelectAccount);
			
				
				JLabel lblEnterSum = new JLabel("Enter sum:");
				lblEnterSum.setBounds(185, 209, 88, 20);
				panel.add(lblEnterSum);
				
				JTextField textField_2 = new JTextField();
				textField_2.setBounds(347, 206, 194, 26);
				panel.add(textField_2);
				textField_2.setColumns(10);
				
				JButton btnWithdraw = new JButton("Withdraw");
				btnWithdraw.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						Person person = bank.findPerson((String) comboBox.getSelectedItem());
						Account acc = bank.findAccount((String) comboBox_1.getSelectedItem());
						acc.addObserver(person);
						if (acc instanceof SavingAccount){
							if(((SavingAccount) acc).getStatus().equals("Open")){
								acc.withdraw(Float.parseFloat(textField_2.getText()));
								((SavingAccount) acc).setStatus("Closed");
							}	
							else
								JOptionPane.showMessageDialog(frame  , "This account is closed.");
						}
						else
							acc.withdraw(Float.parseFloat(textField_2.getText()));
						bank.serialize();
					}
				});
				btnWithdraw.setBounds(305, 294, 115, 29);
				panel.add(btnWithdraw);
				
				JButton btnAdd = new JButton("Add");
				btnAdd.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						Person person = bank.findPerson((String) comboBox.getSelectedItem());
						Account acc = bank.findAccount((String) comboBox_1.getSelectedItem());
						acc.addObserver(person);
						if (acc instanceof SavingAccount){
							if(((SavingAccount) acc).getStatus().equals("Open")){
								acc.withdraw(Float.parseFloat(textField_2.getText()));
								((SavingAccount) acc).setStatus("Closed");
							}	
							else
								JOptionPane.showMessageDialog(frame  , "This account is closed.");
						}
						else
							acc.add(Float.parseFloat(textField_2.getText()));
						bank.serialize();
					}
					
				});
				btnAdd.setBounds(305, 355, 115, 29);
				panel.add(btnAdd);
				frame.setVisible(true);
			}
			
		});
		button.setBounds(187, 240, 179, 49);
		contentPane.add(button);
	}
}
