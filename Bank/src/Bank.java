import java.awt.List;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Bank implements BankProc {
	private HashMap<Person, ArrayList<Account>> map;
	
	Bank(){
		this.map = new HashMap<>();
		try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         this.map = (HashMap) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i) {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c) {
	         System.out.println("Employee class not found");
	         c.printStackTrace();
	         return;
	      }
	}
	public boolean wellFormed(){
		if(map != null)
			return true;
		else
			return false;
	}
	
	public HashMap<Person, ArrayList<Account>> getMap(){
		return this.map;
	}

	public void addPerson(Person person) {
		assert person.getName() != null && person.getAddress() != null & person.getPhoneno() != null;
		assert wellFormed();
		int presize = map.size();
		this.map.put(person, new ArrayList<Account>());
		int postsize = map.size();
		assert postsize == presize + 1;
		
	}

	public void removePerson(Person person) {
		assert wellFormed();
		assert person.getName() != null && person.getAddress() != null & person.getPhoneno() != null;
		int presize = map.size();
		this.map.remove(person);
		int postsize = map.size();
		assert postsize == presize - 1;
		
	}

	public void addAccount(Person person, Account account) {
		assert person.getName() != null && person.getAddress() != null & person.getPhoneno() != null;
		assert wellFormed();
		assert account.getNumber() != null && account.getAmount() >= 0 & account.getType() != null;
		int presize = map.get(person).size();
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			if(entry.getKey().equals(person)){
				entry.getValue().add(account);
			}
		}
		int postsize = map.get(person).size();
		assert postsize == presize + 1 ;
	}

	public void removeAccount(Account account) {
		assert account.getNumber() != null && account.getAmount() >= 0 & account.getType() != null;
		assert wellFormed();
		System.out.println("ayudfeavd" + account.getNumber());
		int presize = 0; int postsize = 0;
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			for (Iterator<Account> iterator = entry.getValue().iterator(); iterator.hasNext(); ) {
			    Account value = iterator.next();
			    if (value.equals(account)) {
			    	presize = entry.getValue().size();
			        iterator.remove();
			        postsize = entry.getValue().size();
			    }
			}
		}
		assert postsize == presize - 1 ;
	}
	
	public void serialize(){
		 try
         {
                FileOutputStream fileOut = new FileOutputStream("bank.ser");
                ObjectOutputStream fout = new ObjectOutputStream(fileOut);
                fout.writeObject(this.map);
                fout.close();
                fileOut.close();
                //System.out.printf("Serialized HashMap data is saved in hashmap.ser");
         }catch(IOException ioe)
          {
                ioe.printStackTrace();
          }
	}
	
	public void fillComboBoxPers(JComboBox comboBox){
		comboBox.removeAllItems();
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			comboBox.addItem(entry.getKey().getName());
		}
	}
	
	public void fillComboBoxAcc(JComboBox comboBox, String name){
		comboBox.removeAllItems();
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			if(entry.getKey().getName().equals(name))
				for(Account acc: entry.getValue())
					comboBox.addItem(acc.getNumber());
		}
	}
	
	public DefaultTableModel buildTableModel(JTable table){
		Vector<Vector<Object>> data = new Vector<>();
		Vector<String> columnNames = new Vector<>();
		columnNames.add("Name");
		columnNames.add("Address");
		columnNames.add("Phone number");
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			Vector<Object> row = new Vector<>();
			row.add(entry.getKey().getName());
			row.add(entry.getKey().getAddress());
			row.add(entry.getKey().getPhoneno());
			data.add(row);
		}
		return new DefaultTableModel(data, columnNames);
	}
	
	public DefaultTableModel buildTableModel2(JTable table){
		Vector<Vector<Object>> data = new Vector<>();
		Vector<String> columnNames = new Vector<>();
		columnNames.add("Owner");
		columnNames.add("Account Number");
		columnNames.add("Amount");
		columnNames.add("Type");
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			Vector<Object> row = null;
			for(Account acc: entry.getValue()){
				if(acc != null){
					row = new Vector<>();
					row.add(entry.getKey().getName());
					row.add(acc.getNumber());
					row.add(acc.getAmount());
					row.add(acc.getType());
					data.add(row);
				}
			}
		}
		return new DefaultTableModel(data, columnNames);
	}
	
	public Person findPerson(String name){
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			if(entry.getKey().getName().equals(name))
				return entry.getKey();
			
		}
		return null;
	}
	
	public Account findAccount(String number){
		for( HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet() ){
			for(Account acc: entry.getValue())
				if(acc.getNumber().equals(number)){
					return acc;
				}
		}
		return null;
	}
	
	

}
