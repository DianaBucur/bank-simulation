
public interface BankProc {
	
	/**
	 * 
	 * @pre person != null
	 * @post map.getsize() = map.getsize()@pre + 1
	 */
	public void addPerson(Person person);
	
	/**
	 * 
	 * @pre person != null
	 * @post map.getsize() = map.getsize()@pre  - 1
	 */
	public void removePerson(Person person);
	
	/**
	 * 
	 * @pre person != null && account != null
	 * @param map.get(person).getsize() = map.get(person).getsize()@pre + 1
	 */
	public void addAccount(Person person, Account account);
	
	/**
	 * 
	 * @pre account != null
	 * @post map.get(person).getsize() = map.get(person).getsize()@pre - 1
	 */
	public void removeAccount(Account account);
}
