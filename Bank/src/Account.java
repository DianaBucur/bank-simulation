import java.io.Serializable;
import java.util.Observable;

public class Account extends Observable implements Serializable {
	private String number;
	private float amount;
	private String type;

	Account(String number, float amount, String type){
		this.number = number;
		this.amount = amount;
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
		setChanged();
		notifyObservers(new Float(amount));
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	public void withdraw(float sum){
		this.setAmount(this.amount - sum);
	}
	
	public void add(float sum){
		this.setAmount(this.amount + sum);
	}
}
