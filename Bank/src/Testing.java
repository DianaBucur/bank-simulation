import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

public class Testing {

	@Test
	public void test() {
		Bank bank = new Bank();
		Person person = new Person("Oliver Heldens", "The Long Street", "0784153942");
		SavingAccount account = new SavingAccount("RO87451344", 10000, "Saving Account", "Open");
		SpendingAccount account1 = new SpendingAccount("RO87412388", 900, "Spending Account");
		bank.addPerson(person);
		bank.addAccount(person, account);
		bank.addAccount(person, account1);
		HashMap<Person, ArrayList<Account>> map = bank.getMap();
		for(HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet()){
 			if(entry.getKey().equals(person))
 				assertEquals(entry.getKey(), person);
 		}
 		assertEquals(map.get(person).get(0), account);
 		assertEquals(map.get(person).get(1), account1);
 		bank.serialize();
	}
	
	@Test
	public void test2(){
		Bank bank = new Bank();
		Account account1 = bank.findAccount("RO87412388");
		bank.removeAccount(account1);
		HashMap<Person, ArrayList<Account>> map = bank.getMap();
		for(HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet())
 			for(Account acc: entry.getValue())
 				assertNotSame(acc, account1);
		bank.serialize();
	}
	
	@Test 
	public void test3(){
		Bank bank = new Bank();
		Person person = bank.findPerson("Oliver Heldens");
		bank.removePerson(person);
		HashMap<Person, ArrayList<Account>> map = bank.getMap();
		for(HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet())
 			assertNotSame(entry.getKey(), person);
		bank.serialize();
	}

}
