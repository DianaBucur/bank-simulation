
public class SavingAccount extends Account {
	private float amount;
	private String number;
	private String type;
	private String status;
	
	SavingAccount(String number, float amount, String type, String status) {
		super(number, amount, type);
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
